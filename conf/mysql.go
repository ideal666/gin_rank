package conf

import (
	"github.com/spf13/viper"
	"strconv"
)

type Mysql struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Database string `yaml:"database"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Config   string `yaml:"config"`
}

func (m Mysql) Dsn() string {
	m.Host = viper.GetString("mysql.host")
	m.Port = viper.GetInt("mysql.port")
	m.Database = viper.GetString("mysql.database")
	m.User = viper.GetString("mysql.user")
	m.Password = viper.GetString("mysql.password")
	m.Config = viper.GetString("mysql.config")
	//"wise888:Ltline2022@tcp(rm-bp131039276s5l855yo.mysql.rds.aliyuncs.com:3306)/go_rank?charset=utf8mb4&parseTime=True&loc=Local"
	return m.User + ":" + m.Password + "@tcp(" + m.Host + ":" + strconv.Itoa(m.Port) + ")/" + m.Database + "?" + m.Config
}
