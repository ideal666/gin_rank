package service

import (
	"context"
	"github.com/redis/go-redis/v9"
	"rank/util"
	"time"
)

type Redis struct{}

// Set string
func (r Redis) Set(key string, value interface{}, second time.Duration) error {
	ctx := context.Background()
	return util.Rdb.Set(ctx, key, value, second).Err()
}

// Get string
func (r Redis) Get(key string) (string, error) {
	ctx := context.Background()
	val, err := util.Rdb.Get(ctx, key).Result()
	return val, err
}

func (r Redis) Del(key string) *redis.IntCmd {
	ctx := context.Background()
	val := util.Rdb.Del(ctx, key)
	return val
}
