package service

import (
	"gorm.io/gorm"
	"rank/model"
	"rank/util"
)

type UserService struct{}

func (u UserService) GetDetailById(id int) (model.User, *gorm.DB) {
	var user model.User
	err := util.Db.Where("id=?", id).First(&user)
	return user, err
}

// GetList 获取列表
func (u UserService) GetList() ([]model.User, int64, error) {
	var list []model.User
	err := util.Db.Find(&list).Error
	var count int64
	util.Db.Count(&count)
	return list, count, err
}

// Register 新增用户
func (u UserService) Register(username string, phone string, password string) (int, error) {
	user := model.User{Username: username, Phone: phone, Password: password, CreatedAt: util.Time(), UpdatedAt: util.Time()}
	err := util.Db.Create(&user).Error
	return user.Id, err
}

// CountByUsername 查询用户是否存在
func (u UserService) CountByUsername(username string) int64 {
	var uid int64
	util.Db.Model(&model.User{}).Where("username=?", username).Count(&uid)
	return uid
}

// GetUser 通过用户名获取用户信息
func (u UserService) GetUser(params map[string]interface{}) (model.User, error) {
	var user model.User
	err := util.Db.Where("username=?", params["username"]).First(&user).Error
	return user, err
}
