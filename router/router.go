package router

import (
	"github.com/gin-gonic/gin"
	"rank/controller"
)

func Router() *gin.Engine {
	r := gin.Default()
	err := r.SetTrustedProxies([]string{"127.0.0.1"})
	if err != nil {
		return nil
	}

	//demo
	demo := r.Group("/demo")
	{
		demo.GET("", controller.DemoController{}.GetList)
		demo.GET("/detail", controller.DemoController{}.GetDetail)
		demo.POST("", controller.DemoController{}.Add)
		demo.POST("/insert", controller.DemoController{}.Insert)
		demo.POST("/insert2", controller.DemoController{}.Insert2)
		demo.PUT("", controller.DemoController{}.Update)
		demo.DELETE("", controller.DemoController{}.Delete)
	}

	//index
	index := r.Group("/index")
	{
		index.GET("test", controller.IndexController{}.Test)
	}

	//login
	login := r.Group("/login")
	{
		login.POST("", controller.LoginController{}.Register)
		login.POST("/login", controller.LoginController{}.Login)
		login.POST("/register", controller.LoginController{}.Register)
	}

	//demo
	user := r.Group("/user")
	{
		user.GET("", controller.UserController{}.GetList)
		user.GET("/getUserById", controller.UserController{}.GetUserById)
	}
	//订单管理
	order := r.Group("/order")
	{
		order.GET("", controller.OrderController{}.GetList)
		order.GET("/detail", controller.OrderController{}.GetDetail)
		order.POST("", controller.OrderController{}.Add)
		order.PUT("", controller.OrderController{}.Update)
		order.DELETE("", controller.OrderController{}.Delete)
	}

	return r
}
