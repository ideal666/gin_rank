package main

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"rank/router"
	"rank/util"
)

func init() {
	util.InitApp()
	//fmt.Println(conf.Mysql{}.Dsn())
	//链接数据库
	util.InitMysql()
	//链接redis
	util.InitRedis()
}

func main() {

	r := router.Router()
	port := viper.GetString("server.port")
	mode := viper.GetString("server.mode")

	gin.SetMode(mode)
	var err = r.Run(":" + port)
	if err != nil {
		return
	}
}
