package util

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type JsonStruct struct {
	Code int         `json:"code"`
	Msg  interface{} `json:"msg"`
	Data interface{} `json:"data"`
}

type JsonErrorStruct struct {
	Code int         `json:"code"`
	Msg  interface{} `json:"msg"`
}

func ReturnOk(c *gin.Context, data interface{}, code int, msg interface{}) {
	json := &JsonStruct{
		Code: code,
		Msg:  msg,
		Data: data,
	}
	c.JSON(http.StatusOK, json)
}

func ReturnError(c *gin.Context, msg interface{}, code int) {
	json := &JsonErrorStruct{
		Code: code,
		Msg:  msg,
	}
	c.JSON(http.StatusOK, json)
}
