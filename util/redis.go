package util

import (
	"github.com/redis/go-redis/v9"
	"github.com/spf13/viper"
)

var Rdb *redis.Client

func InitRedis() {
	Rdb = redis.NewClient(&redis.Options{
		Addr:     viper.GetString("redis.host") + ":" + viper.GetString("redis.port"),
		Password: viper.GetString("redis.auth"),
		DB:       viper.GetInt("redis.db_id"),
	})
}
