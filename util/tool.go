package util

import (
	"crypto/md5"
	"encoding/hex"
	"math/rand"
	"time"
)

// Md5 md5加密
func Md5(s string) string {
	ctx := md5.New()
	ctx.Write([]byte(s))
	return hex.EncodeToString(ctx.Sum(nil))
}

// Time 获取当前时间戳
func Time() int64 {
	return time.Now().Unix()
}

// TimeNano 获取毫秒时间戳
func TimeNano() int64 {
	return time.Now().UnixNano()
}

// RandInt 0-99999生成随机数
func RandInt() int {
	return rand.Intn(99999)
}
