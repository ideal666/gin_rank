package util

import (
	"fmt"
	"github.com/spf13/viper"
)

func InitApp() {
	//加载config文件
	viper.SetConfigName("config")
	viper.AddConfigPath("./conf")
	viper.SetConfigType("yaml")
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			fmt.Println("配置文件为找到")
		} else {
			fmt.Println("配置文件出错...")
		}
	}
	fmt.Println(viper.AllKeys())
}
