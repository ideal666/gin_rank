package util

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"rank/conf"
)

var Db *gorm.DB
var err error

func InitMysql() {
	conf.Mysql{}.Dsn()
	//dsn := "wise888:Ltline2022@tcp(rm-bp131039276s5l855yo.mysql.rds.aliyuncs.com:3306)/go_rank?charset=utf8mb4&parseTime=True&loc=Local"
	Db, err = gorm.Open(mysql.Open(conf.Mysql{}.Dsn()), &gorm.Config{NamingStrategy: schema.NamingStrategy{SingularTable: true}})
	//NamingStrategy: schema.NamingStrategy{SingularTable: true}  使用单数表名
	//Db, err = gorm.Open(mysql.Open(conf.Mysql{}.Dsn()), &gorm.Config{NamingStrategy: schema.NamingStrategy{SingularTable: true}})
	Db = Db.Debug()
	if err == nil || Db.Error == nil {
		fmt.Println("链接成功")
	} else {
		if err != nil {
			fmt.Printf("mysql connect error %v", err)
		}

		if Db.Error != nil {
			fmt.Printf("database error %v", Db.Error)
		}
	}

	//sqlDB, err := Db.DB()

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	//sqlDB.SetMaxIdleConns(10)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	//sqlDB.SetMaxOpenConns(100)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	//sqlDB.SetConnMaxLifetime(time.Hour)
}
