package model

type Order struct {
	Id        int    `json:"id"`
	Uid       int    `json:"uid"`
	OrderNo   string `json:"order_no"`
	CreatedAt int64  `json:"created_at"`
	UpdatedAt int64  `json:"updated_at"`
	DeletedAt int64  `json:"deleted_at"`
}

func (order Order) TableName() string {
	return "order"
}
