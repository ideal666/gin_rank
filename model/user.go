package model

type User struct {
	Id        int    `json:"id"`
	Username  string `json:"username"`
	Phone     string `json:"phone"`
	Password  string `json:"password"`
	CreatedAt int64  `json:"created_at"`
	UpdatedAt int64  `json:"updated_at"`
	DeletedAt int64  `json:"deleted_at"`
}

type UserLogin struct {
	Id        int    `json:"id"`
	Username  string `json:"username"`
	Phone     string `json:"phone"`
	CreatedAt int64  `json:"created_at"`
}

func (user User) TableName() string {
	return "user"
}
