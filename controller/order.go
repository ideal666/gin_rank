package controller

import (
	"github.com/gin-gonic/gin"
	"rank/util"
)

type OrderController struct{}

func (o OrderController) GetList(c *gin.Context) {
	util.ReturnOk(c, "order list", 0, "success")
}

func (o OrderController) GetDetail(c *gin.Context) {
	util.ReturnOk(c, "order detail", 0, "success")
}

func (o OrderController) Add(c *gin.Context) {
	util.ReturnOk(c, "order add", 0, "success")
}

func (o OrderController) Update(c *gin.Context) {
	util.ReturnOk(c, "order update", 0, "success")
}

func (o OrderController) Delete(c *gin.Context) {
	util.ReturnOk(c, "order delete", 0, "success")
}
