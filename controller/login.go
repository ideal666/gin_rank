package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"rank/service"
	"rank/util"
	"strconv"
)

type LoginController struct{}

func (login LoginController) Register(c *gin.Context) {
	params := make(map[string]interface{})
	err := c.BindJSON(&params)
	//fmt.Printf("username 的数据类型%T : ", params["username"])
	if err != nil {
		util.ReturnError(c, err.Error(), -1)
		return
	}
	username := params["username"].(string)
	if username == "" {
		util.ReturnError(c, "username do not blank", -1)
		return
	}
	phone := params["phone"].(string)
	if phone == "" {
		util.ReturnError(c, "phone do not blank", -1)
		return
	}
	password := params["password"].(string)
	if password == "" {
		util.ReturnError(c, "password do not blank", -1)
		return
	}
	repassword := params["repassword"].(string)
	if repassword == "" {
		util.ReturnError(c, "repassword do not blank", -1)
		return
	}

	if repassword != password {
		util.ReturnError(c, "password and repassword do not match", -1)
		return
	}
	//用户名是否已经存在
	count := service.UserService{}.CountByUsername(username)
	fmt.Println("count:", count)
	if count > 0 {
		util.ReturnError(c, "用户名已存在", -1)
		return
	}
	//注册用户
	uid, err := service.UserService{}.Register(username, phone, util.Md5(password))
	if err != nil {
		return
	}

	data := make(map[string]interface{})
	data["uid"] = uid
	data["params"] = params

	util.ReturnOk(c, data, 0, "success")
}

func (login LoginController) Login(c *gin.Context) {
	params := make(map[string]interface{})
	err := c.BindJSON(&params)
	if err != nil {
		util.ReturnError(c, err.Error(), -1)
		return
	}
	username := params["username"].(string)
	if username == "" {
		util.ReturnError(c, "用户名不能为空", -1)
		return
	}
	password := params["password"].(string)
	if password == "" {
		util.ReturnError(c, "密码不能为空", -1)
		return
	}
	user, _ := service.UserService{}.GetUser(params)
	if user.Id == 0 {
		util.ReturnError(c, "用户不存在", -1)
		return
	}
	if util.Md5(password) != user.Password {
		util.ReturnError(c, "用户名或密码有误", -1)
		return
	}
	tokenStr := strconv.Itoa(int(util.TimeNano())) + strconv.Itoa(util.RandInt())
	fmt.Println(tokenStr)
	token := util.Md5(strconv.Itoa(int(util.TimeNano())) + strconv.Itoa(util.RandInt()))

	util.Rdb.Set(c, "rank:login:token", token, 3600*2)
	var data = make(map[string]interface{})
	data["userinfo"] = user
	data["token"] = token
	util.ReturnOk(c, data, 0, "success")
	return
}
