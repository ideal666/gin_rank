package controller

import (
	"github.com/gin-gonic/gin"
	"rank/service"
	"rank/util"
	"strconv"
)

type UserController struct{}

func (u UserController) GetUserById(c *gin.Context) {
	id, _ := strconv.Atoi(c.Query("id"))

	detail, err := service.UserService{}.GetDetailById(id)
	if err.Error != nil {
		util.ReturnError(c, "fail", -1)
	}
	util.ReturnOk(c, detail, 0, "success")
}

// GetList 用户列表
func (u UserController) GetList(c *gin.Context) {
	list, count, _ := service.UserService{}.GetList()
	data := make(map[string]interface{})
	data["list"] = list
	data["count"] = count
	util.ReturnOk(c, data, 0, "success")
}
