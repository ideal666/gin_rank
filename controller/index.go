package controller

import (
	"github.com/gin-gonic/gin"
	"rank/service"
	"rank/util"
)

type IndexController struct{}

func (index IndexController) Test(c *gin.Context) {

	rdberr := service.Redis{}.Set("uid", 1, 0)
	if rdberr != nil {
		util.ReturnError(c, rdberr, -1)
	}
	val, _ := service.Redis{}.Get("uid")

	util.ReturnOk(c, val, 0, "success")
}
