package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"rank/util"
	"strconv"
)

type DemoController struct{}

func (u DemoController) GetList(c *gin.Context) {
	util.ReturnOk(c, "GetUserList", 0, "success")
}

func (u DemoController) GetDetail(c *gin.Context) {
	id := c.Query("id")
	name := c.Query("name")
	fmt.Println(id)
	//name := c.Param("name")
	util.ReturnOk(c, id+"_"+name, 0, "success")
}

// 定义map变量接收POST提交的json数据
func (u DemoController) Insert(c *gin.Context) {
	//定义map变量接受
	params := make(map[string]interface{})
	err := c.BindJSON(&params)
	if err == nil {
		util.ReturnOk(c, params["name"], 0, "success")
		return
	}
	util.ReturnError(c, "获取信息错误", -1)
}

// 定义结构体接收POST提交的json数据
type Demo struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

func (u DemoController) Insert2(c *gin.Context) {
	//
	user := &Demo{}
	err := c.BindJSON(&user)
	fmt.Println(user)
	if err == nil {
		util.ReturnOk(c, user, 0, "success")
		return
	}
	util.ReturnError(c, "获取信息错误", -1)
}

func (u DemoController) Add(c *gin.Context) {
	name := c.PostForm("name")
	userType := c.DefaultPostForm("user_type", strconv.Itoa(1))
	fmt.Println(userType)
	util.ReturnOk(c, name, 0, "success")
}

func (u DemoController) Update(c *gin.Context) {
	util.ReturnOk(c, "UpdateUser", 1, "success")
}

func (u DemoController) Delete(c *gin.Context) {
	util.ReturnError(c, "error", -1)
}
